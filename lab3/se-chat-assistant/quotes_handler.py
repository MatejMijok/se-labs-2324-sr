from query_handler_base import QueryHandlerBase
import requests

class QuoteHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "quote" in query:
            return True
        else:
            return False
        
    def process(self, query):
        
        url = "https://quotes-by-api-ninjas.p.rapidapi.com/v1/quotes"

        headers = {
            "X-RapidAPI-Key": "8377aa1e07msh7ecdf0552d852c8p1a8ea4jsn4784004f7905",
            "X-RapidAPI-Host": "quotes-by-api-ninjas.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)
        data = response.json()
        self.ui.say(f"{data[0]['quote']}\n -{data[0]['author']}")
        