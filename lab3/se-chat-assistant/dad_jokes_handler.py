from query_handler_base import QueryHandlerBase
import requests

class DadJokeHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "dad" in query:
            return True
        else:
            return False

    def process(self, query):

        url = "https://dad-jokes.p.rapidapi.com/random/joke"

        headers = {
            "X-RapidAPI-Key": "8377aa1e07msh7ecdf0552d852c8p1a8ea4jsn4784004f7905",
            "X-RapidAPI-Host": "dad-jokes.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)
        data = response.json()
        self.ui.say("Here's a dad joke: ")
        self.ui.say(data['body'][0]['setup'])
        self.ui.say(data['body'][0]['punchline'])
        
        
        