from query_handler_base import QueryHandlerBase
import requests

class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "weather" in query:
            return True
        else:
            return False
        

    def process(self, query):
        
        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        headers = {
            "X-RapidAPI-Key": "8377aa1e07msh7ecdf0552d852c8p1a8ea4jsn4784004f7905",
            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"
        }

        weather_params = query.split()
        
        if len(weather_params) == 1:
            self.ui.say("Missing city name!")
        
        elif len(weather_params)  == 2:
            querystring = {"q":weather_params[1]}
            response = requests.get(url, headers=headers, params=querystring)
            data = response.json()
            self.ui.say(f"The temperature in {weather_params[1]} is {data['current']['temp_c']} and the condition is {data['current']['condition']['text'].lower()}")
            