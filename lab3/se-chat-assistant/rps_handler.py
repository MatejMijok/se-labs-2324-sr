from query_handler_base import QueryHandlerBase
import random

class RockPaperScissors(QueryHandlerBase):
    def can_process_query(self, query):
        if "rps" in query:
            return True
        else:
            return False

    def process(self, query):
        self.ui.say("Let's play Rock, Paper, Scissors!")
        self.ui.say("Input your choice!")
        
        userAnswer = input()
        assistantAnswer = random.choice(["Rock", "Paper", "Scissors"])
        
        if(userAnswer.lower() == assistantAnswer.lower()):
            self.ui.say("Tie!")
        
        elif(userAnswer.lower() == "paper" and assistantAnswer.lower() == "rock"):
            self.ui.say("You win!") 
        elif(userAnswer.lower() == "scissors" and assistantAnswer.lower() == "paper"):
            self.ui.say("You win!")
        elif(userAnswer.lower() == "rock" and assistantAnswer.lower() == "scissors"):
            self.ui.say("You win!")
        else:
            self.ui.say("You lose!")
