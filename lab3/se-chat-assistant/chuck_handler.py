from query_handler_base import QueryHandlerBase
import requests

class ChuckHandler(QueryHandlerBase):
    
    def can_process_query(self, query):
        if "chuck" in query:
            return True
        else:
            return False


    def process(self, query):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
            "accept": "application/json",
            "X-RapidAPI-Key": "8377aa1e07msh7ecdf0552d852c8p1a8ea4jsn4784004f7905",
            "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)
        data = response.json()
        
        self.ui.say("OK, here's one:")
        self.ui.say(data['value'])
