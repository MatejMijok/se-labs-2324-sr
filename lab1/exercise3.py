def return_check(list):
    """Return true or false if the list contains a 2 next to a 2.

    Arguments: 
    list - input list of integers which will be checked
    """

    numbers = [int(number) for number in list.split(",")]

    for i in range(len(numbers)-1):
        if numbers[i] == 2 and numbers[i+1] == 2:
            return True
    
    return False
    


     

def main():
    print(return_check("1, 2, 2, 4, 100"))
    print(return_check("1, 1, 5, 5, 10, 8, 7"))
    print(return_check("-10, -4, -2, -4 , -2, 0"))

if __name__ == "__main__":
    main()