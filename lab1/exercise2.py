def centered_average(list):
    """Return the average of the values excluding the smallest and largest value.

    Arguments: 
    list - input list of integers which will be checked
    """
    list.remove(min(list))
    list.remove(max(list))
    
    avg = sum(list) // len(list)

    return avg

def main():
    print(centered_average([1, 2, 3, 4, 100]))
    print(centered_average([1, 1, 5, 5, 10, 8, 7]))
    print(centered_average([-10, -4, -2, -4 , -2, 0]))

if __name__ == "__main__":
    main()