def char_types_count(list):
    """Return the number of uppercase and lowercase letters and numbers

    Arguments: 
    list - input list of integers which will be checked
    """
    uppercase_count = 0
    lowercase_count = 0
    number_count = 0

    for character in list:
        if character.isdigit():
            number_count += 1
        if character.isupper():
            uppercase_count +=1
        if character.islower():
            lowercase_count += 1
    
    return (uppercase_count, lowercase_count, number_count)
    

def main():
    print(char_types_count("asdf98CXX21grrr"))

if __name__ == "__main__":
    main()