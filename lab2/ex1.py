def main():
    name = input("What is your name? ")
    
    print(f"Hi {name}")
    print("Hi " + name)
    print("Hi {}".format(name))

if __name__ == "__main__":
    main()